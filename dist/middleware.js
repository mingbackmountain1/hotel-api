"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
function needAuthorization(request, response, next) {
    try {
        const secretKey = process.env.APP_KEY;
        const [_, token] = request.headers.authorization.split(" ");
        const payload = jsonwebtoken_1.default.verify(token, secretKey);
        request.$payload = payload;
        next();
    }
    catch (err) {
        response.status(401).json({
            error: err.message
        });
    }
}
exports.needAuthorization = needAuthorization;
function withHotelId(request, response, next) {
    request.$hotelId = request.$payload.hotelId;
    next();
}
exports.withHotelId = withHotelId;
