"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const BookingHistoryRepository_1 = require("../features/hotel/domain/repositories/BookingHistoryRepository");
const CheckInHistoryRepository_1 = require("../features/hotel/domain/repositories/CheckInHistoryRepository");
const KeyCardRepository_1 = require("../features/hotel/domain/repositories/KeyCardRepository");
const UserRepository_1 = require("../features/authentication/domain/repositories/UserRepository");
const RoomRepository_1 = require("../features/hotel/domain/repositories/RoomRepository");
const HotelRepository_1 = require("../features/hotel/domain/repositories/HotelRepository");
const typeorm_1 = require("typeorm");
const bcrypt_1 = __importDefault(require("bcrypt"));
class BookingService {
    async createHotel(floorCount, roomPerFloorCount, email, password) {
        const hotelRepository = typeorm_1.getCustomRepository(HotelRepository_1.HotelRepository);
        const keyCardRepository = typeorm_1.getCustomRepository(KeyCardRepository_1.KeyCardRepository);
        const roomRepository = typeorm_1.getCustomRepository(RoomRepository_1.RoomRepository);
        const userRepository = typeorm_1.getCustomRepository(UserRepository_1.UserRepository);
        const hotelId = await hotelRepository.createHotel(floorCount, roomPerFloorCount);
        const saltRound = 10;
        const passHash = bcrypt_1.default.hashSync(password, saltRound);
        await userRepository.createUser(hotelId, email, passHash);
        await keyCardRepository.createKeyCards(hotelId, floorCount * roomPerFloorCount);
        await roomRepository.createRooms(hotelId, floorCount, roomPerFloorCount);
        return hotelId;
    }
    async findUser(email, password) {
        const userRepository = typeorm_1.getCustomRepository(UserRepository_1.UserRepository);
        const user = await userRepository.getUser(email, password);
        return user;
    }
    async bookAndCheckIn(hotelId, roomNo, guest) {
        const roomRepository = typeorm_1.getCustomRepository(RoomRepository_1.RoomRepository);
        const bookingHistoryRepository = typeorm_1.getCustomRepository(BookingHistoryRepository_1.BookingHistoryRepository);
        const keyCardRepository = typeorm_1.getCustomRepository(KeyCardRepository_1.KeyCardRepository);
        const checkInHistoryRepository = typeorm_1.getCustomRepository(CheckInHistoryRepository_1.CheckInHistoryRepository);
        const room = await roomRepository.getRoomByNo(hotelId, roomNo);
        if (room.isBooked) {
            const bookingHistory = await bookingHistoryRepository.getBookingHistoryByRoomNo(hotelId, roomNo);
            throw new Error(`Cannot book room ${roomNo} for ${guest.name}, The room is currently booked by ${bookingHistory.name}`);
        }
        await roomRepository.book(room);
        const bookingHistory = await bookingHistoryRepository.createBookingHistory(hotelId, room, guest);
        await bookingHistoryRepository.checkIn(bookingHistory);
        const borrowedKeyCard = await keyCardRepository.borrowKeyCard(hotelId);
        const checkInHistory = await checkInHistoryRepository.createCheckInHistory(hotelId, bookingHistory, guest, borrowedKeyCard);
        await checkInHistoryRepository.save(checkInHistory);
        return { bookingHistory, checkInHistory };
    }
    async bookByFloorAndCheckIn(hotelId, floor, guest) {
        const roomsInFloor = await this.getRoomsByFloor(hotelId, floor);
        const isRoomsAllAvaliable = roomsInFloor.every(room => !room.isBooked);
        const roomRepository = typeorm_1.getCustomRepository(RoomRepository_1.RoomRepository);
        const bookingHistoryRepository = typeorm_1.getCustomRepository(BookingHistoryRepository_1.BookingHistoryRepository);
        const checkInHistoryRepository = typeorm_1.getCustomRepository(CheckInHistoryRepository_1.CheckInHistoryRepository);
        const keyCardRepository = typeorm_1.getCustomRepository(KeyCardRepository_1.KeyCardRepository);
        if (!isRoomsAllAvaliable) {
            throw new Error(`Cannot book floor ${floor} for ${guest.name}.`);
        }
        const bookedRoomInFloor = await Promise.all(roomsInFloor.map(room => roomRepository.book(room)));
        const bookingHistories = await bookingHistoryRepository.createBookingHistories(hotelId, bookedRoomInFloor, guest);
        const customerNotCheckIn = bookingHistories.filter(bookingHistory => !bookingHistory.isCheckIn);
        const isAllRoomsBooked = customerNotCheckIn.every(bookingHistory => bookingHistory.isCheckIn);
        if (isAllRoomsBooked) {
            throw new Error(`Guest already booked all rooms in floor.`);
        }
        await Promise.all(bookingHistories.map(bookingHistory => bookingHistoryRepository.checkIn(bookingHistory)));
        const checkInHistories = await checkInHistoryRepository.createCheckInHistories(hotelId, customerNotCheckIn, guest, keyCardRepository, bookingHistoryRepository);
        return { bookingHistories, checkInHistories };
    }
    async checkOutGuestByFloor(hotelId, floor) {
        const checkInHistoryRepository = typeorm_1.getCustomRepository(CheckInHistoryRepository_1.CheckInHistoryRepository);
        const roomRepository = typeorm_1.getCustomRepository(RoomRepository_1.RoomRepository);
        const checkInHistoryByFloor = await checkInHistoryRepository.getCheckInHistoriesByFloor(hotelId, floor);
        if (!checkInHistoryRepository.isGuestCheckedIn(checkInHistoryByFloor)) {
            throw new Error("Guest can't checkout rooms by floor");
        }
        const roomNos = checkInHistoryRepository.getRoomNosFromCheckInHistories(checkInHistoryByFloor);
        const rooms = await Promise.all(roomNos.map(no => roomRepository.getRoomByNo(hotelId, no)));
        checkInHistoryByFloor.map((checkInHistory, index) => {
            this.clearRoom(hotelId, rooms[index], checkInHistory.keycard_no);
        });
        return rooms;
    }
    async checkOutRoom(hotelId, guestName, keyCardNo) {
        const checkInHistoryRepository = typeorm_1.getCustomRepository(CheckInHistoryRepository_1.CheckInHistoryRepository);
        const roomRepository = typeorm_1.getCustomRepository(RoomRepository_1.RoomRepository);
        const checkInHistories = await checkInHistoryRepository.getCheckInHistoriesByGuestName(hotelId, guestName);
        const checkInHistory = await checkInHistoryRepository.getCheckInHistoryByKeycard(checkInHistories, keyCardNo);
        if (!checkInHistoryRepository.isGuestCheckedIn(checkInHistory)) {
            const currentGuestBookedRoom = await checkInHistoryRepository.getCheckInHistoriesByKeyCardNo(hotelId, keyCardNo);
            throw new Error(`Only ${currentGuestBookedRoom.name} can checkout with keycard number ${currentGuestBookedRoom.keycard_no}.`);
        }
        const room = await roomRepository.getRoomByNo(hotelId, checkInHistory.room_no);
        await this.clearRoom(hotelId, room, keyCardNo);
        return { room: room };
    }
    async listAvailableRooms(hotelId) {
        const roomRepository = typeorm_1.getCustomRepository(RoomRepository_1.RoomRepository);
        return await roomRepository.availableRooms(hotelId);
    }
    async listCheckInGuest(hotelId) {
        const checkInHistoryRepository = typeorm_1.getCustomRepository(CheckInHistoryRepository_1.CheckInHistoryRepository);
        return await checkInHistoryRepository.getGuestInCheckInHistories(hotelId);
    }
    async listGuestByAge(hotelId, condition, age) {
        const checkInHistoryRepository = typeorm_1.getCustomRepository(CheckInHistoryRepository_1.CheckInHistoryRepository);
        return await checkInHistoryRepository.getGuestByCondition(hotelId, checkInHistory => {
            switch (condition) {
                case "<":
                    return checkInHistory.age < age;
                case ">":
                    return checkInHistory.age > age;
                case "=":
                    return checkInHistory.age === age;
                default:
                    return;
            }
        });
    }
    async getGuestInRoom(hotelId, roomNo) {
        const checkInHistoryRepository = typeorm_1.getCustomRepository(CheckInHistoryRepository_1.CheckInHistoryRepository);
        const guest = await checkInHistoryRepository.getGuestByRoomNo(hotelId, roomNo);
        if (guest === undefined)
            throw new Error("guest not found");
        return guest;
    }
    async listGuest(hotelId) {
        return await this.listCheckInGuest(hotelId);
    }
    async listGuestByFloor(hotelId, floor) {
        const checkInHistoryRepository = typeorm_1.getCustomRepository(CheckInHistoryRepository_1.CheckInHistoryRepository);
        return await checkInHistoryRepository.getGuestByCondition(hotelId, checkInHistory => checkInHistory.floor === parseInt(floor));
    }
    async clearRoom(hotelId, room, keyCardNo) {
        const keyCardRepository = typeorm_1.getCustomRepository(KeyCardRepository_1.KeyCardRepository);
        const roomRepository = typeorm_1.getCustomRepository(RoomRepository_1.RoomRepository);
        const checkInHistoryRepository = typeorm_1.getCustomRepository(CheckInHistoryRepository_1.CheckInHistoryRepository);
        const bookingHistoryRepository = typeorm_1.getCustomRepository(BookingHistoryRepository_1.BookingHistoryRepository);
        await roomRepository.unbook(room);
        await keyCardRepository.returnKeyCard(hotelId, keyCardNo);
        await checkInHistoryRepository.clearCheckInHistoryByKeyCardNo(hotelId, keyCardNo);
        await bookingHistoryRepository.clearBookingHistory(hotelId, room.no);
    }
    async getRoomsByFloor(hotelId, floor) {
        const roomRepository = typeorm_1.getCustomRepository(RoomRepository_1.RoomRepository);
        const rooms = await roomRepository.getRoomByFloor(hotelId, floor);
        return rooms;
    }
}
exports.BookingService = BookingService;
exports.bookingService = new BookingService();
