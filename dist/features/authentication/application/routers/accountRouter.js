"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const AccountController_1 = require("../../../../controllers/AccountController");
exports.accountRouter = express_1.Router();
exports.accountRouter.post("/register", AccountController_1.register);
exports.accountRouter.post("/signIn", AccountController_1.signIn);
