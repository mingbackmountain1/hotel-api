"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class PostHotels {
}
__decorate([
    class_validator_1.IsInt(),
    class_validator_1.Min(1),
    class_validator_1.Max(10),
    __metadata("design:type", Number)
], PostHotels.prototype, "floor", void 0);
__decorate([
    class_validator_1.IsInt(),
    class_validator_1.Min(1),
    class_validator_1.Max(10),
    __metadata("design:type", Number)
], PostHotels.prototype, "roomPerFloor", void 0);
__decorate([
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], PostHotels.prototype, "email", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.Length(8),
    __metadata("design:type", String)
], PostHotels.prototype, "password", void 0);
exports.PostHotels = PostHotels;
