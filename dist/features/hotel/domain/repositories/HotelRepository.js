"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Hotel_1 = require("../entities/Hotel");
let HotelRepository = class HotelRepository extends typeorm_1.Repository {
    async createHotel(floorCount, roomPerFloorCount) {
        const hotel = this.create({
            floor: floorCount,
            room_per_floor: roomPerFloorCount
        });
        await this.save(hotel);
        return hotel.id.toString();
    }
};
HotelRepository = __decorate([
    typeorm_1.EntityRepository(Hotel_1.Hotel)
], HotelRepository);
exports.HotelRepository = HotelRepository;
