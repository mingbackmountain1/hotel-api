"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const KeyCard_1 = require("../entities/KeyCard");
const typeorm_1 = require("typeorm");
let KeyCardRepository = class KeyCardRepository extends typeorm_1.Repository {
    async getKeycard(hotelId, keyCardNo) {
        try {
            const keycard = await this.findOneOrFail({
                no: keyCardNo,
                hotel_id: hotelId
            });
            return keycard;
        }
        catch (err) {
            console.log("keycard not find");
        }
    }
    async createKeyCards(hotelId, keycardCount) {
        const keycards = Array.from({ length: keycardCount }, (_, KeyCardindex) => {
            const KeyCardNo = KeyCardindex + 1;
            return this.create({
                no: KeyCardNo,
                hotel_id: hotelId
            });
        }).reverse();
        this.save(keycards);
        return keycards;
    }
    async borrowKeyCard(hotelId) {
        const keycard = await this.findOneOrFail({
            where: {
                hotel_id: hotelId
            },
            order: {
                no: "ASC"
            }
        });
        await this.remove(keycard);
        return keycard;
    }
    async borrowKeyCards(hotelId, roomCount) {
        const keycards = await this.find({
            where: {
                hotel_id: hotelId
            },
            order: {
                no: "ASC"
            },
            take: roomCount
        });
        await this.remove(keycards);
        return keycards;
    }
    async returnKeyCard(hotelId, keyCardNo) {
        const keycard = await this.create({
            no: keyCardNo,
            hotel_id: hotelId
        });
        this.save(keycard);
    }
};
KeyCardRepository = __decorate([
    typeorm_1.EntityRepository(KeyCard_1.KeyCard)
], KeyCardRepository);
exports.KeyCardRepository = KeyCardRepository;
