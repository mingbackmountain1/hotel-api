"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const CheckInHistory_1 = require("../entities/CheckInHistory");
const Guest_1 = require("../entities/Guest");
const typeorm_1 = require("typeorm");
let CheckInHistoryRepository = class CheckInHistoryRepository extends typeorm_1.Repository {
    async listCheckInHistories(hotelId) {
        const checkInHistories = this.find({ hotel_id: hotelId });
        return checkInHistories;
    }
    async getGuestInCheckInHistories(hotelId) {
        const checkInHistories = await this.find({ hotel_id: hotelId });
        const guests = checkInHistories.map(checkInHistory => new Guest_1.Guest(checkInHistory.name, checkInHistory.age));
        return guests;
    }
    async clearCheckInHistoryByKeyCardNo(hotelId, keyCardNo) {
        let checkInHistories = await this.listCheckInHistories(hotelId);
        const removeCheckIn = checkInHistories.filter(checkInHistory => {
            return (checkInHistory.keycard_no === keyCardNo &&
                checkInHistory.hotel_id === hotelId);
        });
        await this.remove(removeCheckIn);
        return checkInHistories;
    }
    getCheckInHistoryByKeycard(checkInHistories, keyCardNo) {
        return checkInHistories.find(checkInHistory => checkInHistory.keycard_no === keyCardNo);
    }
    async getCheckInHistoriesByKeyCardNo(hotelId, keyCardNo) {
        const checkInHistories = await this.listCheckInHistories(hotelId);
        return await checkInHistories.find(checkInHistory => checkInHistory.keycard_no === keyCardNo);
    }
    async getCheckInHistoriesByGuestName(hotelId, guestName) {
        const checkInHistories = await this.listCheckInHistories(hotelId);
        return checkInHistories.filter(checkInHistory => checkInHistory.name === guestName);
    }
    async getCheckInHistoriesByFloor(hotelId, floor) {
        const checkInHistories = await this.listCheckInHistories(hotelId);
        return checkInHistories.filter(checkInHistory => checkInHistory.floor === floor);
    }
    getRoomNosFromCheckInHistories(checkInHistories) {
        return checkInHistories.map(checkInHistory => checkInHistory.room_no);
    }
    async getGuestByRoomNo(hotelId, roomNo) {
        const guest = await this.getGuestByCondition(hotelId, checkInHistory => checkInHistory.room_no === roomNo);
        return guest[0];
    }
    async getGuestByCondition(hotelId, condition) {
        const checkInHistories = await this.listCheckInHistories(hotelId);
        const guests = checkInHistories
            .filter(condition)
            .map(checkInHistory => new Guest_1.Guest(checkInHistory.name, checkInHistory.age));
        return guests;
    }
    createCheckInHistory(hotelId, bookingHistory, guest, keyCard) {
        const checkInHistory = this.create({
            name: guest.name,
            age: guest.age,
            room_no: bookingHistory.room_no,
            floor: bookingHistory.floor,
            hotel_id: hotelId,
            keycard_no: keyCard.no
        });
        return checkInHistory;
    }
    async createCheckInHistories(hotelId, bookingHistories, guest, keyCardManager, bookingHistoryManager) {
        const keycards = await keyCardManager.borrowKeyCards(hotelId, bookingHistories.length);
        const checkInHistories = await Promise.all(bookingHistories.map(async (bookingHistory, index) => {
            const bookingHistoryCheckIn = await bookingHistoryManager.checkIn(bookingHistory);
            const checkInHistory = await this.createCheckInHistory(hotelId, bookingHistoryCheckIn, guest, keycards[index]);
            await this.save(checkInHistory);
            return checkInHistory;
        }));
        return checkInHistories;
    }
    isGuestCheckedIn(checkInHistory) {
        return checkInHistory !== undefined;
    }
};
CheckInHistoryRepository = __decorate([
    typeorm_1.EntityRepository(CheckInHistory_1.CheckInHistory)
], CheckInHistoryRepository);
exports.CheckInHistoryRepository = CheckInHistoryRepository;
