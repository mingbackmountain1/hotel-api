"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Room_1 = require("../entities/Room");
const typeorm_1 = require("typeorm");
let RoomRepository = class RoomRepository extends typeorm_1.Repository {
    async createRooms(hotelId, floorCount, roomPerFloorCount) {
        const rooms = Array.from({ length: floorCount }, (_, indexFloor) => Array.from({ length: roomPerFloorCount }, (_, indexRoom) => {
            const roomNo = indexRoom + 1;
            const floorNo = indexFloor + 1;
            return this.create({
                no: floorNo.toString() + roomNo.toString().padStart(2, "0"),
                floor: floorNo,
                isBooked: false,
                hotelId: hotelId
            });
        })).flat();
        await this.save(rooms);
        return rooms;
    }
    async book(room) {
        room.isBooked = true;
        await this.save(room);
        return room;
    }
    async unbook(room) {
        room.isBooked = false;
        await this.save(room);
        return room;
    }
    async getRoomByFloor(hotelId, floor) {
        const rooms = await this.find({
            floor: floor,
            hotelId: hotelId
        });
        return rooms;
    }
    async getRoomByNo(hotelId, roomNo) {
        return await this.findOneOrFail({
            no: roomNo,
            hotelId: hotelId
        });
    }
    async availableRooms(hotelId) {
        return this.find({
            where: { hotelId: hotelId, isBooked: false },
            order: { no: "ASC" }
        });
    }
};
RoomRepository = __decorate([
    typeorm_1.EntityRepository(Room_1.Room)
], RoomRepository);
exports.RoomRepository = RoomRepository;
