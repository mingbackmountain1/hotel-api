"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const BookingHistory_1 = require("../entities/BookingHistory");
const typeorm_1 = require("typeorm");
let BookingHistoryRepository = class BookingHistoryRepository extends typeorm_1.Repository {
    async createBookingHistories(hotelId, rooms, guest) {
        const bookingHistories = await Promise.all(rooms.map(room => {
            return this.createBookingHistory(hotelId, room, guest);
        }));
        return bookingHistories;
    }
    async getBookingHistoryByRoomNo(hotelId, roomNo) {
        return await this.findOneOrFail({
            room_no: roomNo,
            hotel_id: hotelId
        });
    }
    async createBookingHistory(hotelId, room, guest) {
        const bookingHistory = this.create({
            name: guest.name,
            age: guest.age,
            room_no: room.no,
            isCheckIn: false,
            floor: room.floor,
            hotel_id: hotelId
        });
        return bookingHistory;
    }
    async checkIn(bookingHistory) {
        bookingHistory.isCheckIn = true;
        await this.save(bookingHistory);
        return bookingHistory;
    }
    async listBookingHistories() {
        const bookingHistories = await this.find();
        return bookingHistories;
    }
    async clearBookingHistory(hotelId, roomNo) {
        const bookingHistories = await this.listBookingHistories();
        const deletBookingHistories = bookingHistories.filter(bookingHistory => bookingHistory.room_no === roomNo && bookingHistory.hotel_id === hotelId);
        this.remove(deletBookingHistories);
    }
};
BookingHistoryRepository = __decorate([
    typeorm_1.EntityRepository(BookingHistory_1.BookingHistory)
], BookingHistoryRepository);
exports.BookingHistoryRepository = BookingHistoryRepository;
