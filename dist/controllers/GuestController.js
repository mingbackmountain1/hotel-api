"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BookingService_1 = require("../service/BookingService");
const class_transformer_validator_1 = require("class-transformer-validator");
const GetGuestByFloor_1 = require("../features/hotel/application/posts/GetGuestByFloor");
const GetGuestByAge_1 = require("../features/hotel/application/posts/GetGuestByAge");
async function listGuest(request, response) {
    const hotelId = request.$hotelId;
    const guests = await BookingService_1.bookingService.listGuest(hotelId);
    response.json({ result: guests });
}
exports.listGuest = listGuest;
async function listGuestByAge(request, response) {
    try {
        const { symbol, age } = (await class_transformer_validator_1.transformAndValidate(GetGuestByAge_1.GetGuestByAge, request.params));
        const hotelId = request.$hotelId;
        const guest = await BookingService_1.bookingService.listGuestByAge(hotelId, symbol, age);
        response.json({ result: guest });
    }
    catch (error) {
        console.log(error);
        response.send(error.message);
    }
}
exports.listGuestByAge = listGuestByAge;
async function listGuestByFloor(request, response) {
    try {
        const { floor } = (await class_transformer_validator_1.transformAndValidate(GetGuestByFloor_1.GetGuestByFloor, request.params));
        const hotelId = request.$hotelId;
        const guestByFloor = await BookingService_1.bookingService.listGuestByFloor(hotelId, floor);
        response.json({ result: guestByFloor });
    }
    catch (error) {
        response.send(error.message);
    }
}
exports.listGuestByFloor = listGuestByFloor;
