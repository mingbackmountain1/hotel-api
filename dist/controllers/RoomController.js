"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BookingService_1 = require("../service/BookingService");
const Guest_1 = require("../features/hotel/domain/entities/Guest");
const class_transformer_validator_1 = require("class-transformer-validator");
const GetGuestInRoom_1 = require("../features/hotel/application/posts/GetGuestInRoom");
const PostRoomBook_1 = require("../features/hotel/application/posts/PostRoomBook");
const PostRoomCheckOut_1 = require("../features/hotel/application/posts/PostRoomCheckOut");
const PostBookByFloor_1 = require("../features/hotel/application/posts/PostBookByFloor");
const PostCheckOutByFloor_1 = require("../features/hotel/application/posts/PostCheckOutByFloor");
async function book(request, response) {
    const { roomNo, guestName, guestAge } = (await class_transformer_validator_1.transformAndValidate(PostRoomBook_1.PostRoomBook, request.body));
    const guest = new Guest_1.Guest(guestName, guestAge);
    const hotelId = request.$hotelId;
    try {
        const confirmationInfo = await BookingService_1.bookingService.bookAndCheckIn(hotelId, roomNo, guest);
        response.json({ result: confirmationInfo });
    }
    catch (error) {
        response.send(error.message);
    }
}
exports.book = book;
async function checkout(request, response) {
    try {
        const { keyCardNo, guestName } = (await class_transformer_validator_1.transformAndValidate(PostRoomCheckOut_1.PostRoomCheckOut, request.body));
        const hotelId = request.$hotelId;
        const room = await BookingService_1.bookingService.checkOutRoom(hotelId, guestName, keyCardNo);
        response.json({ result: room });
    }
    catch (error) {
        response.send(error.message);
    }
}
exports.checkout = checkout;
async function bookByFloor(request, response) {
    try {
        const { floor, guestName, guestAge } = (await class_transformer_validator_1.transformAndValidate(PostBookByFloor_1.PostBookByFloor, request.body));
        const hotelId = request.$hotelId;
        const guest = new Guest_1.Guest(guestName, guestAge);
        const confirmationInfo = await BookingService_1.bookingService.bookByFloorAndCheckIn(hotelId, floor, guest);
        response.json({ result: confirmationInfo });
    }
    catch (error) {
        console.log(error);
        response.send(error.message);
    }
}
exports.bookByFloor = bookByFloor;
async function checkOutByFloor(request, response) {
    try {
        const { floor } = (await class_transformer_validator_1.transformAndValidate(PostCheckOutByFloor_1.PostCheckOutByFloor, request.body));
        const hotelId = request.$hotelId;
        const rooms = await BookingService_1.bookingService.checkOutGuestByFloor(hotelId, floor);
        response.json({ result: rooms });
    }
    catch (error) {
        response.send(error.message);
    }
}
exports.checkOutByFloor = checkOutByFloor;
async function availableRooms(request, response) {
    const hotelId = request.$hotelId;
    const available_rooms = await BookingService_1.bookingService.listAvailableRooms(hotelId);
    response.json({ result: available_rooms });
}
exports.availableRooms = availableRooms;
async function getGuestInRoom(request, response) {
    try {
        const { roomId } = (await class_transformer_validator_1.transformAndValidate(GetGuestInRoom_1.GetGuestInRoom, request.params));
        const hotelId = request.$hotelId;
        const guest = await BookingService_1.bookingService.getGuestInRoom(hotelId, roomId);
        response.json({ result: { name: guest.name } });
    }
    catch (error) {
        response.send(error.message);
    }
}
exports.getGuestInRoom = getGuestInRoom;
