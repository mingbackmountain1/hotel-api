"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const BookingService_1 = require("../service/BookingService");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const class_transformer_validator_1 = require("class-transformer-validator");
const PostHotels_1 = require("../features/authentication/application/posts/PostHotels");
const PostSignIn_1 = require("../features/authentication/application/posts/PostSignIn");
const secretKey = process.env.APP_KEY;
async function register(request, response) {
    try {
        const { floor, roomPerFloor, email, password } = (await class_transformer_validator_1.transformAndValidate(PostHotels_1.PostHotels, request.body));
        const hotel = await BookingService_1.bookingService.createHotel(floor, roomPerFloor, email, password);
        const token = jsonwebtoken_1.default.sign({
            hotelId: hotel
        }, secretKey, { algorithm: "HS256", expiresIn: "1h", issuer: "Hotel" });
        response.json({ result: token });
    }
    catch (error) {
        response.status(500).json(error);
    }
}
exports.register = register;
async function signIn(request, response) {
    try {
        const { email, password } = (await class_transformer_validator_1.transformAndValidate(PostSignIn_1.PostSignIn, request.body));
        const user = await BookingService_1.bookingService.findUser(email, password);
        const token = jsonwebtoken_1.default.sign({
            userId: user.id
        }, secretKey, { algorithm: "HS256", expiresIn: "1h", issuer: "Hotel" });
        response.json({ result: token });
    }
    catch (error) {
        response.status(500).json(error);
    }
}
exports.signIn = signIn;
