"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const fs_1 = require("fs");
function migrateDatabase() {
    const sqlFiles = fs_1.readdirSync("migrations");
    const sqlCommands = sqlFiles.map(file => {
        const sqlCommand = fs_1.readFileSync("migrations" + "/" + file, "utf8");
        return sqlCommand;
    });
    sqlCommands.forEach(command => {
        typeorm_1.getConnection().query(command);
    });
}
exports.migrateDatabase = migrateDatabase;
