"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = require("dotenv");
dotenv_1.config();
const migrateDatabase_1 = require("./migrateDatabase");
const typeorm_1 = require("typeorm");
const app_1 = require("./app");
typeorm_1.createConnections().then(() => {
    migrateDatabase_1.migrateDatabase();
    const port = process.env.PORT || 3000;
    app_1.app.listen(port, () => console.log(`Server is running at ${port}`));
});
