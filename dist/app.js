"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const roomRouter_1 = require("./features/hotel/application/routers/roomRouter");
const guestRouter_1 = require("./features/hotel/application/routers/guestRouter");
const accountRouter_1 = require("./features/authentication/application/routers/accountRouter");
exports.app = express_1.default();
exports.app.use(express_1.default.json());
exports.app.use("/auth", accountRouter_1.accountRouter);
exports.app.use("/rooms", roomRouter_1.roomRouter);
exports.app.use("/guests", guestRouter_1.guestRouter);
